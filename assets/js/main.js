$( document ).ready(function() {

	// WOW
	wow = new WOW({
		live: true,
	}).init();

	// SMOOTH SCROLL
	$(".backtotop").click(function(e) {
		$('html,body').animate({scrollTop: 0},'slow');
	});

	// CLOSE BT
	$("#contato input[type='submit']").click(function(e){
		e.preventDefault();
		$('#contato .mensagem').addClass('show');
	});


	
});